package com.example.trabalhocontrutoramatheus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView imoveis;
    private ImageView contatos;
    private ImageView simulacao;
    private ImageView inventidores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imoveis = findViewById(R.id.IdImoveis);
        contatos = findViewById(R.id.IdContatos);
        inventidores = findViewById(R.id.IdInvestidores);
        simulacao = findViewById(R.id.IdSimulacao);

        imoveis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, imoveis.class));

            }
        });

        contatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, contatos.class));
            }
        });
        simulacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, simulacao.class));
            }
        });



        inventidores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, investimentos.class));
            }
        });
    }
}
